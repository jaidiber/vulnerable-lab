from flask import Flask, request
import requests
from flask_cors import CORS
import mysql.connector
import subprocess
import json

app = Flask(__name__)
CORS(app)

def call_mysql_instance():
    mydb = mysql.connector.connect(
    host="localhost",
    user="ft",
    password="root",
    database='world'
    )
    return mydb

@app.route('/', methods=['GET'])
def index():
    return '<h1> Welcome</h1> \
        <a href="/file">Empecemos por el principio</a> \
        <br> \
        <a href="/search/country">Información de paises</a> \
        </form>'


@app.route('/file', methods=['POST'])
def get_request():
    try:
        file_request = request.form['file']
        cat_command = 'cat '
        command_complete = cat_command + file_request
        proccess_result = subprocess.run(command_complete, shell=True, capture_output=True)
        proccess_result = (proccess_result.stdout).decode()
        json_record = {'message': str(proccess_result)}
    except Exception as error:
        json_record = {'error': str(error)}
    return json_record

@app.route('/country', methods=['POST'])
def get_country():
    try:
        mydb = call_mysql_instance()
        country_name = request.form['name']
        query = "select * from country where name='"+str(country_name)+"'"
        mycursor = mydb.cursor()
        mycursor.execute(query)
        record = mycursor.fetchall()
        mydb.close()
        if record == []:
            json_record = {'Error': 'Deje la pereza e ingresa algo ome!'}
        else:
            json_record = {
                'Información': str(record)
            }
    except Exception as error:
        json_record = {'error': 'No me veas, soy un feo error!! '+ str(error)}
    return json_record

@app.route("/search/country", methods=['GET'])
def search_country():
    return '<h1>Buscador de información de paises</h1> \
        <form action="/country" method="POST"> \
        <input name="name"> \
        <input type="submit" value="Buscar"> \
        </form>\
        <a href="/">atrás</a>'

@app.route("/file", methods=['GET'])
def search_file():
    return '<h1> Hola, por favor carga el mensaje de bienvenida</h1> \
        <form action="/file" method="POST"> \
        <input name="file" value="HelloWorld.txt" placeholder="HelloWorld.txt" readonly=true> \
        <input type="submit" value="Buscar"> \
        </form>\
        <a href="/">atrás</a>'

if __name__ == '__main__':
    app.run('0.0.0.0', 5000, debug=True)