FROM python:3.8-buster

ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8

WORKDIR /app
COPY . /app
RUN apt-get update
RUN apt-get install mariadb-server mariadb-client -y
RUN pip install virtualenv
RUN virtualenv env
RUN . env/bin/activate
RUN pip install -r requirements.txt
ENV FLASK_APP=main.py

ENTRYPOINT ["sh", "execute.sh"]
EXPOSE 5000